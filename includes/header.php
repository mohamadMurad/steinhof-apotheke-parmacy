<!DOCTYPE html>
<html style="font-size: 16px;">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta name="keywords" content="MEDICINE CENTER ON THE GO!">
  <meta name="description" content="">
  <meta name="page_type" content="np-template-header-footer-from-plugin">
  <title>Page</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">



  <link rel="stylesheet" href="./css/nicepage.css?version=51146633-6fb5-43c2-8a77-bafc58f8f9a2" media="screen">
  <script class="u-script" type="text/javascript" src="./js/jquery-1.9.1.min.js" defer=""></script>
  <script class="u-script" type="text/javascript" src="./js/nicepage.js" defer=""></script>


  <link id="u-theme-google-font" rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
  <link id="u-page-google-font" rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700">

  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/custom.css">


  <meta property="og:title" content="Page">
  <meta property="og:type" content="website">
  <meta name="theme-color" content="#73a3de">
  <link rel="canonical" href="#">
  <meta property="og:url" content="#">

</head>

<body class="u-body">
  <header class="u-clearfix u-header u-palette-1-base u-header" id="sec-39b6">
    <div class="u-clearfix u-sheet u-sheet-1">
      <div class="d-flex justify-content-between align-items-center ">
        <a href="index.php" class="u-image u-logo u-image-1" src="">
          <img src="./img/logo.svg" class="u-logo-image u-logo-image-1" width="64" height="64">
        </a>
        <nav class="u-align-left u-menu u-menu-dropdown u-nav-spacing-25 u-offcanvas u-menu-1" data-responsive-from="LG">
          <div class="menu-collapse">
            <a class="u-btn-text u-button-style u-nav-link u-text-body-alt-color" href="#"
              style="padding: 4px 0px; font-size: calc(1em + 8px);">
              <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 302 302" style="undefined">
                <use xlink:href="#svg-7b92"></use>
              </svg>
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                id="svg-7b92" x="0px" y="0px" viewBox="0 0 302 302" style="enable-background:new 0 0 302 302;"
                xml:space="preserve" class="u-svg-content">
                <g>
                  <rect y="36" width="302" height="30"></rect>
                  <rect y="236" width="302" height="30"></rect>
                  <rect y="136" width="302" height="30"></rect>
                </g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
                <g></g>
              </svg>
            </a>
          </div>
          <div class="u-nav-container">
            <ul class="u-nav u-spacing-25 u-unstyled">
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="/Page-1.html"
                  data-page-id="16719" style="padding: 8px 0px;">Über uns</a>
              </li>
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="./vorbestellung.php"
                data-page-id="16719" style="padding: 8px 0px;">Vorbestellung</a>
            </li>
            <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="./lieferservice.php"
              data-page-id="16719" style="padding: 8px 0px;">Lieferservice</a>
          </li>
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="#"
                  data-page-id="41668" style="padding: 8px 0px;">Leistungen</a>
              </li>
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="#"
                  data-page-id="41669" style="padding: 8px 0px;">Eigenkosmatik</a>
              </li>
  
            </ul>
          </div>
          <div class="u-nav-container-collapse">
            <div
              class="u-align-center u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
              <div class="u-inner-container-layout u-sidenav-overflow">
                <div class="u-menu-close"></div>
                <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2">
                <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="/Page-1.html"
                  data-page-id="16719" style="padding: 8px 0px;">Über uns</a>
              </li>
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="./vorbestellung.php"
                data-page-id="16719" style="padding: 8px 0px;">Vorbestellung</a>
            </li>
            <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="./lieferservice.php"
              data-page-id="16719" style="padding: 8px 0px;">Lieferservice</a>
          </li>
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="#"
                  data-page-id="41668" style="padding: 8px 0px;">Leistungen</a>
              </li>
              <li class="u-nav-item"><a class="u-btn-text u-button-style u-nav-link" href="#"
                  data-page-id="41669" style="padding: 8px 0px;">Eigenkosmatik</a>
              </li>
  
                </ul>
              </div>
            </div>
            <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
          </div>
          <style class="offcanvas-style">
            .u-offcanvas .u-sidenav {
              flex-basis: 250px !important;
            }
  
            .u-offcanvas:not(.u-menu-open-right) .u-sidenav {
              margin-left: -250px;
            }
  
            .u-offcanvas.u-menu-open-right .u-sidenav {
              margin-right: -250px;
            }
  
            @keyframes menu-shift-left {
              from {
                left: 0;
              }
  
              to {
                left: 250px;
              }
            }
  
            @keyframes menu-unshift-left {
              from {
                left: 250px;
              }
  
              to {
                left: 0;
              }
            }
  
            @keyframes menu-shift-right {
              from {
                right: 0;
              }
  
              to {
                right: 250px;
              }
            }
  
            @keyframes menu-unshift-right {
              from {
                right: 250px;
              }
  
              to {
                right: 0;
              }
            }
          </style>
        </nav>
      
      </div>
    </div>
  
  </header>


<section class="u-backlink u-clearfix u-grey-80">

<div class="row footer-address u-sheet u-sheet-1">
  <div class="col-12 col-md-4">
    <div class="d-flex flex-column justify-content-center align-items-center">
      <div class="d-flex align-items-center justify-content-center gap-4">
        <span class="u-icon u-icon-circle u-text-white u-icon-1">
          <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 513.64 513.64" style="">
            <use xlink:href="#svg-d5b6"></use>
          </svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
            xml:space="preserve" class="u-svg-content" viewBox="0 0 513.64 513.64" x="0px" y="0px" id="svg-d5b6"
            style="enable-background:new 0 0 513.64 513.64;">
            <g>
              <g>
                <path
                  d="M499.66,376.96l-71.68-71.68c-25.6-25.6-69.12-15.359-79.36,17.92c-7.68,23.041-33.28,35.841-56.32,30.72    c-51.2-12.8-120.32-79.36-133.12-133.12c-7.68-23.041,7.68-48.641,30.72-56.32c33.28-10.24,43.52-53.76,17.92-79.36l-71.68-71.68    c-20.48-17.92-51.2-17.92-69.12,0l-48.64,48.64c-48.64,51.2,5.12,186.88,125.44,307.2c120.32,120.32,256,176.641,307.2,125.44    l48.64-48.64C517.581,425.6,517.581,394.88,499.66,376.96z">
                </path>
              </g>
            </g>
          </svg>
        </span>
        <h3 class="u-text u-text-body-alt-color u-text-4">Kontaktdaten</h3>
      </div>
      <p class="u-text u-text-body-alt-color u-text-5">
        <span><b>Tel.</b>: 05254/5293</span>
        <span> <b>Fax</b>: 05254/60987</span>
        <span><b>Email</b>: <a href="mailto:Steinhof-Apotheke@web.de">Steinhof-Apotheke@web.de</a></span>
        <span></span>
        <span></span>
        <span></span>
      </p>
    </div>
  </div>
  <div class="col-12 col-md-4">
    <div class="d-flex flex-column justify-content-start">
      <div class="d-flex align-items-center justify-content-center gap-4">
        <span class="u-icon u-icon-circle u-text-white u-icon-2"><svg class="u-svg-link"
            preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
            <use xlink:href="#svg-fb66"></use>
          </svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
            xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" x="0px" y="0px" id="svg-fb66"
            style="enable-background:new 0 0 512 512;">
            <g>
              <g>
                <path
                  d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5    c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021    C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333    s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z">
                </path>
              </g>
            </g>
          </svg>
        </span>
        <h3 class="u-text u-text-body-alt-color u-text-6">Adresse</h3>
      </div>
      <p class="u-text u-text-body-alt-color u-text-7 ">
        <span>Steinhof-Apotheke </span>
        <span>Von-Kletterer 51</span>
        <span>33106 Paderborn</span>
      </p>
    </div>
  </div>
  <div class="col-12 col-md-4">
    <div class="d-flex flex-column justify-content-start ">
      <div class="d-flex align-items-center justify-content-center gap-4">
        <span class="u-icon u-icon-circle u-text-white u-icon-3"><svg class="u-svg-link"
            preserveAspectRatio="xMidYMin slice" viewBox="0 0 512 512" style="">
            <use xlink:href="#svg-27f6"></use>
          </svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
            xml:space="preserve" class="u-svg-content" viewBox="0 0 512 512" id="svg-27f6">
            <g>
              <path
                d="m386.058 256c0-8.284 6.716-15 15-15h31.587c-7.224-85.814-75.831-154.421-161.645-161.645v31.588c0 8.284-6.716 15-15 15s-15-6.716-15-15v-31.588c-85.814 7.224-154.421 75.831-161.645 161.645h31.587c8.284 0 15 6.716 15 15s-6.716 15-15 15h-31.587c7.224 85.814 75.831 154.421 161.645 161.645v-31.588c0-8.284 6.716-15 15-15s15 6.716 15 15v31.588c85.814-7.224 154.421-75.831 161.645-161.645h-31.587c-8.284 0-15-6.716-15-15zm-39.467-71.629-79.838 82.087c-5.558 5.714-14.618 6.086-20.625.835l-59.598-52.101c-6.237-5.452-6.873-14.929-1.42-21.165 5.452-6.237 14.928-6.875 21.166-1.421l48.889 42.739 69.921-71.891c5.776-5.938 15.273-6.069 21.211-.295 5.938 5.778 6.07 15.274.294 21.212z">
              </path>
              <path
                d="m256 0c-141.159 0-256 114.841-256 256s114.841 256 256 256 256-114.841 256-256-114.841-256-256-256zm0 463.286c-114.298 0-207.286-92.988-207.286-207.286s92.988-207.286 207.286-207.286 207.286 92.988 207.286 207.286-92.988 207.286-207.286 207.286z">
              </path>
            </g>
          </svg>
        </span>
        <h3 class="u-text u-text-body-alt-color u-text-8">Öffnungszeiten</h3>
      </div>
      <p class="u-text u-text-body-alt-color u-text-9">
        <span> <b>Montag -Freitag</b> : 8:00-13:00 / 14:30-18:30</span>
        <span>
          <b> Samstag </b>: 8:00-13:00
        </span>
      </p>
    </div>
  </div>
</div>
<div class="copy-right">
  <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
    <span>Website Templates</span>
  </a>
  <p class="u-text">
    <span>created with</span>
  </p>
  <a class="u-link" href="#" target="_blank">
    <span>#</span>
  </a>.
</div>

</section>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
crossorigin="anonymous"></script>
</body>

</html>
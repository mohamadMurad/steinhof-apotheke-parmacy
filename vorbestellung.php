<?php
include_once "includes/header.php";
?>

 

  <section class="u-clearfix u-section-5  vorbestellung" id="sec-4ab9">
    <div class="u-clearfix u-sheet u-sheet-1">
      <div class="u-clearfix u-expanded-width u-gutter-50 u-layout-wrap u-layout-wrap-1">
        <div class="u-layout">
          <div class="u-layout-row">
            <div class="u-container-style u-image u-layout-cell u-left-cell u-size-30 u-image-1">
              <div class="u-container-layout"></div>
            </div>
            <div class="u-container-style u-layout-cell u-right-cell u-size-30 u-layout-cell-2">
              
              <div class="u-form u-form-1">
                <h2 class="u-custom-font u-font-oswald u-text u-text-1">Vorbestellung</h2>
                <p> text ------------------------------------------- text </p>
                <form action="#" method="POST"
                  class="u-block-df76-8 u-clearfix u-form-spacing-20 u-form-vertical u-inner-form" source="email">
                
                  <div class="u-form-group u-form-name">
                    <label for="name-b93e" class="u-form-control-hidden u-label">Vorname</label>
                    <input type="text" placeholder="Vorname" id="name-b93e" name="Vorname"
                      class="u-border-no-bottom u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-palette-5-dark-2"
                      required="">
                  </div>
                  <div class="u-form-group u-form-name">
                    <label for="name-b93e" class="u-form-control-hidden u-label">Nachname</label>
                    <input type="text" placeholder="Nachname" id="name-b93e" name="Nachname"
                      class="u-border-no-bottom u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-palette-5-dark-2"
                      required="">
                  </div>
                  <div class="u-form-group u-form-name">
                    <label for="name-b93e" class="u-form-control-hidden u-label">Telefonnummer</label>
                    <input type="text" placeholder="Telefonnummer" id="name-b93e" name="Telefonnummer"
                      class="u-border-no-bottom u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-palette-5-dark-2"
                      required="">
                  </div>
                  <div class="u-form-email u-form-group">
                    <label for="email-b93e" class="u-form-control-hidden u-label">Email</label>
                    <input type="email" placeholder="Email" id="email-b93e" name="email"
                      class="u-border-no-bottom u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-palette-5-dark-2"
                      required="">
                  </div>
                  <div class="u-form-group u-form-name">
                    <label for="name-b93e" class="u-form-control-hidden u-label">Krankenkasse (falls Sie ein Rezept haben):</label>
                    <input type="text" placeholder="Krankenkasse (falls Sie ein Rezept haben)" id="name-b93e" name="Krankenkasse"
                      class="u-border-no-bottom u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-palette-5-dark-2"
                      required="">
                  </div>
                  <div class="u-form-group u-form-message">
                    <label for="message-b93e" class="u-form-control-hidden u-label">Bitte geben Sie den genauen Text des Rezeptes oder der Arzneimittel ein:</label>
                    <textarea placeholder="Bitte geben Sie den genauen Text des Rezeptes oder der Arzneimittel ein" rows="4" cols="50" id="message-b93e" name="message"
                      class="u-border-no-bottom u-border-no-left u-border-no-right u-border-no-top u-input u-input-rectangle u-palette-5-dark-2"
                      required=""></textarea>
                  </div>
              
                  <div class="form-check u-form-email u-form-group">
                    <input class="form-check-input " type="checkbox" value="" id="flexCheckDisabled" >
                    <label class="form-check-label u-label" for="flexCheckDisabled">
                      Ich willige ein, dass meine oben eingetragenen Daten zum Zwecke der Abwicklung meiner Anfrage verarbeitet werden. Meine Einwilligung kann ich jederzeit widerrufen. Näheres finde ich in der Datenschutzbelehrung. (Siehe Link unten)
                    </label>
                  </div>
                  <div class="u-align-center u-form-group u-form-submit u-form-group-4">
                    <a href="#" class="u-btn u-btn-submit u-button-style u-palette-5-light-2 u-btn-1">Vorbestellung versenden</a>
                    <input type="submit" value="submit" class="u-form-control-hidden">
                  </div>
                  <div class="u-form-send-message u-form-send-success"> Thank you! Your message has been sent. </div>
                  <div class="u-form-send-error u-form-send-message"> Unable to send your message. Please fix errors then try
                    again. </div>
                  <input type="hidden" value="" name="recaptchaResponse">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>



  <?php
include_once "includes/footer.php";
?>